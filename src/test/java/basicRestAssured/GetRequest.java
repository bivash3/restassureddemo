package basicRestAssured;

import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest {

	@Test
	public void restAssuredGet() {
		
		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/9");
		String body = response.asString();
		System.out.println(body);
		//System.out.println(response);
	}
}
