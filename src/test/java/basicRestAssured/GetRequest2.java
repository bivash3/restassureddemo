package basicRestAssured;

import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest2 {

	@Test
	public void restAssuredGet() {
		
		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/9");
		
		int statuscode = response.getStatusCode();
		System.out.println(statuscode);
		
		String statusline = response.getStatusLine();
		System.out.println(statusline);
		
		
	}
}
