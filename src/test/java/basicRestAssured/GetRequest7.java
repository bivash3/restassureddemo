package basicRestAssured;

import java.util.Arrays;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetRequest7 {

	@Test
	public void restAssuredGet() {
		
		// Print all the response headers
		
		RestAssured
		.get("https://restful-booker.herokuapp.com/booking/9")
		.then()
		.statusCode(Matchers.in(Arrays.asList(200, 201)))
		.body("firstname", Matchers.startsWith("S"));
		
		
	}
}
