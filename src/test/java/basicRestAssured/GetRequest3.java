package basicRestAssured;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetRequest3 {

	@Test
	public void restAssuredGet() {
		
		RestAssured
		.get("https://restful-booker.herokuapp.com/booking/9")
		.then()
		.statusCode(200)
		.statusLine("HTTP/1.1 200 OK")
		.body("firstname", Matchers.equalTo("Sally"))
		.body("bookingdates.checkout", Matchers.equalTo("2016-11-12"));
		
		
	}
}
