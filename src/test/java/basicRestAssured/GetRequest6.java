package basicRestAssured;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetRequest6 {

	@Test
	public void restAssuredGet() {
		
		// Print all the response headers
		
		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/9");
		
		Headers headers = response.getHeaders();
		
		for(Header h: headers) {
			System.out.println(h.getName() + " "+ h.getValue());
		}
		
		System.out.println("Single header print: " + response.getHeader("connection"));
	}
}
