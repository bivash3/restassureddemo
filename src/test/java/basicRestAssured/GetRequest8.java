package basicRestAssured;

import java.util.Arrays;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetRequest8 {

	@Test
	public void restAssuredGet() {
		
		// print response as string
		
		Response response = RestAssured.get("https://restful-booker.herokuapp.com/booking/9");
		
		System.out.println(response.asString());
		response.prettyPrint();
		
	}
}
