package basicRestAssured;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetRequest4 {

	@Test
	public void restAssuredGet() {
		
		JsonPath jsonpath = RestAssured
		.get("https://restful-booker.herokuapp.com/booking/9")
		.then()
		.statusCode(200)
		.statusLine("HTTP/1.1 200 OK")
		.extract()
		.jsonPath();
		
		System.out.println(jsonpath.getString("firstname"));
		System.out.println(jsonpath.getString("bookingdates.checkout"));
		//System.out.println(jsonpath);
		
		
	}
}
